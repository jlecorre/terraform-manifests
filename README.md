# Terraform manifests

This repository exists to host several Terraform manifests executed to instantiate OVHcloud services by using the OVHcloud Terraform provider.

## Requirements

- `terraform` binary
- `jq` binary
- `curl` binary
- `gnu-make` binary

## Usage

- By using the Terraform CLI directly
- By using the `Makefile` available in different folders

## Details

Work in progress here :)  
Open PR if you want to contribute!

## Contact

Feel free to reach me on:

- Gitlab directly
- On Twitter: https://twitter.com/joellecorre
- On Mastodon: https://framapiaf.org/@jlecorre