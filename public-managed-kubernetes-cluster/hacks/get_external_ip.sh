#!/usr/bin/env bash

function error_exit() {
  echo "$1" 1>&2
  exit 1
}

function check_dependencies() {
  if ! command -v jq 1>/dev/null 2>&1
  then
    error_exit "JQ binary not found"
  fi
  if ! command -v curl 1>/dev/null 2>&1
  then
    error_exit "curl binary not found"
  fi
}

function extract_data_and_generate_json_result() {
  ip_address=$(curl -qs https://ifconfig.me/ip)
  # shellcheck disable=SC2086
  jq -n --arg ip_address "ip_address" '{"ip_address":"'$ip_address/32'"}'
}

check_dependencies
extract_data_and_generate_json_result