# Sources: https://github.com/ovh/terraform-provider-ovh
terraform {
  required_providers {
    ovh = {
      source  = "ovh/ovh"
      version = "0.28.1"
    }
  }
}

provider "ovh" {
  # Optional - If omitted, the environment variables are used.
  # Values can be exported from password-store entries
  # endpoint           = "<ovh_endpoint>"
  # application_key    = "<your_application-key>"
  # application_secret = "<your_application_secret>"
  # consumer_key       = "<your_consumer_key>"
}
