# Managed Kubernetes Service cluster resource
resource "ovh_cloud_project_kube" "public_mks_dev_cluster" {

  service_name  = var.pci_service_name
  name          = var.mks_service_name
  region        = var.pci_region_name
  version       = var.mks_service_version
  update_policy = var.mks_service_update_policy

  kube_proxy_mode = "iptables"
  customization_kube_proxy {
    iptables {
      sync_period     = "PT1M"
      min_sync_period = "PT20S"
    }
  }

  customization_apiserver {
    admissionplugins {
      enabled  = ["NodeRestriction"]
      disabled = ["AlwaysPullImages"]
    }
  }

  # Custom timeouts applied during create/update/delete of MKS resources
  timeouts {
    create = "30m"
    update = "1h"
    delete = "5m"
  }
}

# Get automatically current IP to allow it in IP restrictions (testing purpose)
# https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/external
data "external" "get_current_ip_address" {
  program = ["/usr/bin/bash", "-c", "hacks/get_external_ip.sh"]
}

# Managed Kubernetes Service cluster IP restriction definition
resource "ovh_cloud_project_kube_iprestrictions" "authorized_access" {
  service_name = var.pci_service_name
  kube_id      = ovh_cloud_project_kube.public_mks_dev_cluster.id
  # Get required information
  ips = [data.external.get_current_ip_address.result.ip_address]
}

# Managed Kubernetes Service nodepool resources
resource "ovh_cloud_project_kube_nodepool" "np-monthly-billed-no-autoscaler" {
  service_name   = var.pci_service_name
  kube_id        = ovh_cloud_project_kube.public_mks_dev_cluster.id
  name           = var.mks_nodepool_name
  flavor_name    = var.mks_flavor_kind
  desired_nodes  = var.mks_desired_nodes
  max_nodes      = var.mks_max_nodes_in_nodepool
  min_nodes      = var.mks_min_nodes_in_nodepool
  anti_affinity  = var.mks_anti_affinity_is_enabled
  monthly_billed = var.mks_monthly_billing_is_enabled
  #  template {
  #    metadata {
  #        var.mks_nodepool_labels
  #    }
  #    spec {
  #      unschedulable = false
  #    }
  #  }
}

# Kubeconfig file management (testing purpose only)
#resource "local_file" "kubeconfig" {
#  content  = ovh_cloud_project_kube.public_mks_dev_cluster.kubeconfig
#  filename = "public_mks_dev_cluster.yml"
#}
