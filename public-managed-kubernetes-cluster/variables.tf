variable "mks_service_name" {
  type        = string
  default     = "mks-terraformed-cluster-no-vrack-iptables-mode"
  description = "Name of the Managed Kubernetes Service cluster to manage"
}

variable "mks_service_version" {
  type    = string
  default = "1.25"
}

variable "mks_service_update_policy" {
  type = string
  # Available values: ALWAYS_UPDATE, MINIMAL_DOWNTIME, NEVER_UPDATE]
  default = "ALWAYS_UPDATE"
}

variable "mks_nodepool_name" {
  type    = string
  default = "nodepool-no-autoscaled"
}

variable "mks_nodepool_labels" {
  type = map(any)
  default = {
    labels = {
      "managed_by_terraform" : "true",
      "purpose" : "testing"
    }
  }
}

variable "mks_flavor_kind" {
  type    = string
  default = "d2-8"
}

variable "mks_desired_nodes" {
  type    = number
  default = 3
}

variable "mks_min_nodes_in_nodepool" {
  type    = number
  default = 1
}

variable "mks_max_nodes_in_nodepool" {
  type    = number
  default = 5
}

variable "mks_autoscale_is_enabled" {
  type    = bool
  default = "true"
}

variable "mks_monthly_billing_is_enabled" {
  type    = bool
  default = "true"
}

variable "mks_anti_affinity_is_enabled" {
  type    = bool
  default = "true"
}

variable "pci_region_name" {
  type    = string
  default = "GRA9"
}

variable "pci_service_name" {
  type    = string
  default = "b2e5936265ad41df9e84f2c684280eee"
}