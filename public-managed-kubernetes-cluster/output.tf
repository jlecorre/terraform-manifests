output "mks_service_id" {
  value = ovh_cloud_project_kube.public_mks_dev_cluster.id
}
output "ip_restrictions_defined" {
  value = data.external.get_current_ip_address.result.ip_address
}

output "get_kubeconfig_file" {
  value     = ovh_cloud_project_kube.public_mks_dev_cluster.kubeconfig
  sensitive = true
}